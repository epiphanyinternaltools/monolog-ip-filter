<?php

namespace Epiphany\MonologIpFilterBundle;

use Epiphany\MonologIpFilterBundle\DependencyInjection\EpiphanyMonologIpFilterExtension;
use Symfony\Bundle\MonologBundle\DependencyInjection\MonologExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Monolog\Formatter\JsonFormatter;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\HandlerInterface;
use Symfony\Bundle\MonologBundle\DependencyInjection\Compiler\AddSwiftMailerTransportPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Bundle\MonologBundle\DependencyInjection\Compiler\LoggerChannelPass;
use Symfony\Bundle\MonologBundle\DependencyInjection\Compiler\DebugHandlerPass;
use Symfony\Bundle\MonologBundle\DependencyInjection\Compiler\AddProcessorsPass;
use Symfony\Bundle\MonologBundle\DependencyInjection\Compiler\FixEmptyLoggerPass;

class EpiphanyMonologIpFilterBundle extends Bundle
{
    /**
     * Set the ContainerExtension
     * se we can provide our own alias and to
     * user 'monolog' for our bundle
     *
     * @return EpiphanyMonologIpFilterExtension
     */
    public function getContainerExtension()
    {
        return new EpiphanyMonologIpFilterExtension();
    }

}

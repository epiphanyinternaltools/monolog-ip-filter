<?php


namespace Epiphany\MonologIpFilterBundle\FingersCrossed;

use Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy;
use Symfony\Bridge\Monolog\Handler\FingersCrossed\NotFoundActivationStrategy;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Activation strategy that ignores errors from Ip ranges
 *
 * @author Dimitris Stoikidis <dimitris.stoikidis@epiphanysolutions.co.uk>
 */
class IpActivationStrategy extends ErrorLevelActivationStrategy
{
    private $blacklist;
    private $requestStack;
    private $existingActivationStrategy;

    public function __construct(RequestStack $requestStack, array $excludedIps, $actionLevel, $existingActivationStrategy)
    {
        parent::__construct($actionLevel);
        $this->blacklist = $excludedIps;
        $this->requestStack = $requestStack;
        $this->existingActivationStrategy = $existingActivationStrategy;
    }

    public function isHandlerActivated(array $record)
    {
        // Existing Activation strategy found found, lets check for them first
        if($this->existingActivationStrategy instanceof ErrorLevelActivationStrategy){
            if(!$this->existingActivationStrategy->isHandlerActivated($record)){
                return false;
            }
        }

        $isActivated = parent::isHandlerActivated($record);

        if (
            $isActivated
            && isset($record['context']['exception'])
            && $record['context']['exception'] instanceof HttpException
            && $record['context']['exception']->getStatusCode() == 404
            && ($request = $this->requestStack->getMasterRequest())
        ) {
            return !IpUtils::checkIp($request->getClientIp(), $this->blacklist);
        }

        return $isActivated;
    }
}

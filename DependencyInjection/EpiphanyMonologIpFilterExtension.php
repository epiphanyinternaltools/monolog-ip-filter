<?php

namespace Epiphany\MonologIpFilterBundle\DependencyInjection;

use Epiphany\MonologIpFilterBundle\FingersCrossed\IpActivationStrategy;
use Symfony\Bundle\MonologBundle\DependencyInjection\MonologExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;


/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class EpiphanyMonologIpFilterExtension extends MonologExtension
{
    const FINGERS_CROSSED = 'fingers_crossed';

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        parent::load($configs, $container);

        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        foreach ($config['handlers'] as $name => $handler) {

            $this->replaceNotFoundStrategy($container, $name, $handler);

            if($this->hasIpFilterConfiguration($handler, $config, $name)){
                $this->switchActivationStrategy($container, $name, $handler);
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     * @param $name
     * @param $handler
     */
    protected function switchActivationStrategy(ContainerBuilder $container, $name, $handler)
    {
        $handlerDefinition = $container->getDefinition(sprintf('monolog.handler.%s', $name));

        // Get the Reference for the NotFoundActivationStrategy
        $existingActivationStrategy = $this->getExistingActivationStrategy($handlerDefinition);

        // Create a new Definition for our new IpFilter ActivationStrategy
        $activationDef = new Definition(IpActivationStrategy::class, array(
            new Reference('request_stack'),
            $handler['excluded_ips'],
            $handler['action_level'],
            $existingActivationStrategy
        ));

        $handlerId = sprintf('monolog.handler.%s', $name) . '.ip_filter_strategy';

        $container->setDefinition($handlerId, $activationDef);
        $activation = new Reference($handlerId);

        // and replace the argument on the handler
        $handlerDefinition->replaceArgument(1, $activation);

    }

    /**
     * Checks to see if the handler type is set to 'fingers_crossed'
     * and the configuration for excluded Ips is not empty
     *
     * @param $handler
     * @param $config
     * @param $name
     * @return bool
     */
    protected function hasIpFilterConfiguration($handler, $config, $name)
    {
        return $handler['type'] === self::FINGERS_CROSSED && !empty($config['handlers'][$name]['excluded_ips']);
    }

    /**
     * Returns the Existing Activation Strategy
     * as defined in configuration
     *
     * @param Definition $handlerDefinition
     * @return Reference
     */
    protected function getExistingActivationStrategy(Definition $handlerDefinition)
    {
        $existingActivationStrategyReference = null;

        if ($handlerDefinition->getArgument(1)) {
            $existingActivationStrategyReference = $handlerDefinition->getArgument(1);
        }

        return $existingActivationStrategyReference;
    }

    /**
     * Fix for older versions of Monolog with Symfony 3
     * https://github.com/symfony/monolog-bundle/issues/166
     *
     * We replace the old NotFoundActivationStrategy from the MonologBundle
     * with the class provided by Symfony Bridget (Symfony >= 2.6)
     *
     * @param ContainerBuilder $container
     * @param $name
     * @param $handler
     */
    protected function replaceNotFoundStrategy(ContainerBuilder $container, $name, $handler)
    {
        $handlerId = 'monolog.handler.' . $name . '.not_found_strategy';

        if($container->hasDefinition($handlerId)){

            $notFoundDefinition = $container->getDefinition($handlerId);
            
            /* 
                DDM - 20171114 - Added an extra check for when getClass() returns an actual class
                as it seems to in SF3.3
            */
            if(class_exists($notFoundDefinition->getClass())) {
                $class = $notFoundDefinition->getClass();
            } else {
                $class = $container->getParameter(str_replace('%', '', $notFoundDefinition->getClass()));
            }

            // Class used for Definition is the old from the MonologBundle, we want to switch to use the SymfonyBridge one
            if($class === "Symfony\Bundle\MonologBundle\NotFoundActivationStrategy" && $notFoundDefinition->hasMethodCall('setRequest')){
                $activationDef = new Definition('Symfony\Bridge\Monolog\Handler\FingersCrossed\NotFoundActivationStrategy', array(
                    new Reference('request_stack'),
                    $handler['excluded_404s'],
                    $handler['action_level']
                ));

                $container->setDefinition($handlerId, $activationDef);
            }

        }

    }

    /**
     * Use Monolog alias so we can have access to the configuration
     *
     * @return string
     */
    public function getAlias()
    {
        return 'monolog';
    }
}

Monolog - Ip Filter Activation Strategy
=====================

Activation strategy that ignores Ips/Ip Ranges.

- Adds an `IP Filter Activation Strategy` to the FingersCrossed Monolog definition
- Existing strategies (custom or excluded_404s) take precedence and will be executed first
- Fixes issue with `excluded_404s` does not work for symfony 3
https://github.com/symfony/monolog-bundle/issues/166

Ip Ranges can be defined using `CIDR notation` https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing


```
Ip Range Examples:
--
194.168.101.34/24 -> 194.168.101.*
194.168.120.1/24 -> 194.168.120.*
--
194.168.120.1/16 -> 194.168.*.*
194.201.20.100/16 -> 194.168.*.*
```

This bundle is also available via [composer](https://github.com/composer/composer), find it on [packagist](https://packagist.org/packages/epiphany/monolog-ip-filter).


## Installation ##

Add the `epiphany/monolog-ip-filter` package to your `require` section in the `composer.json` file.

``` bash
$ composer require epiphany/monolog-ip-filter
```

Add the MonologIpFilterBundle to your application's kernel:

``` php
<?php
public function registerBundles()
{
    $bundles = array(
        // ...
        new Epiphany\MonologIpFilterBundle\EpiphanyMonologIpFilterBundle(),
        // ...
    );
    ...
}
```

## Usage ##

Configure your handlers to use the `IP filter`:

``` yaml
monolog:
    handlers:
        main:
            type:         fingers_crossed
            action_level: error
            handler:      swift
            excluded_ips:
                - 192.168.33.200
                - 194.168.101.34/24
                - 201.31.187.1/16
```

Using IP Filter with `excluded_404s`

``` yaml
monolog:
    handlers:
        main:
            type:         fingers_crossed
            action_level: error
            handler:      swift
            excluded_404s:
                - ^/wp-login.php
                - ^/administrator
            excluded_ips:
                - 192.168.33.200
                - 194.168.101.34/24
                - 201.31.187.1/16
```


Version History
===============
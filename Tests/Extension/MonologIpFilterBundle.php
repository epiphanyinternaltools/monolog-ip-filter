<?php

namespace Epiphany\MonologIpFilterBundle\Tests\Extension;
use Epiphany\MonologIpFilterBundle\DependencyInjection\EpiphanyMonologIpFilterExtension;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;


class MonologIpFilterBundle extends AbstractExtensionTestCase
{
    protected function getContainerExtensions()
    {
        return array(
            new EpiphanyMonologIpFilterExtension()
        );
    }

    /**
     * @test
     */
//    public function after_loading_the_correct_parameter_has_been_set()
//    {
//        $this->load();
//        //$this->assertContainerBuilderHasParameter('parameter_name', 'some value');
//    }

//    public function getAlias()
//    {
//        return 'monolog';
//    }
}
<?php

namespace Epiphany\MonologIpFilterBundle\Tests\FingersCrossed;

use Epiphany\MonologIpFilterBundle\FingersCrossed\IpActivationStrategy;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Monolog\Handler\FingersCrossed\NotFoundActivationStrategy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\DateTime;

class IpActivationStrategyTest extends TestCase
{
    public function testIpFilterWithRanges()
    {
        $requestStack = $this->createMock(RequestStack::class);
        $request = $this->createMock(Request::class);
        $request->method('getClientIp')->willReturn('193.168.10.1');
        $requestStack->method('getMasterRequest')->willReturn($request);

        $ips = [
            '193.168.33.100',
            '193.168.33.101',
            '193.168.33.100',
            '193.168.33.102'
        ];
        $ipActivationStrategy = new IpActivationStrategy($requestStack,$ips,100,null);
        $record = $this->buildRecord();
        $result = $ipActivationStrategy->isHandlerActivated($record);

        // Will return TRUE, Handler will be triggered
        $this->assertTrue($result);

        $ips = [
            '193.168.10.1',
            '193.168.33.101',
            '193.168.33.100',
            '193.168.33.102'
        ];
        $ipActivationStrategy = new IpActivationStrategy($requestStack,$ips,100,null);
        $record = $this->buildRecord();
        $result = $ipActivationStrategy->isHandlerActivated($record);
        // Will return FALSE, Handler will not be triggered
        $this->assertFalse($result);


        $ips = [
            '193.168.2.1/16',
            '193.168.33.20'
        ];
        $ipActivationStrategy = new IpActivationStrategy($requestStack,$ips,100,null);
        $result = $ipActivationStrategy->isHandlerActivated($record);
        // Will return FALSE, Handler will not be triggered
        $this->assertFalse($result);


        $ips = [
            '193.168.33.1/16',
            '193.168.33.20'
        ];
        $ipActivationStrategy = new IpActivationStrategy($requestStack,$ips,100,null);
        $result = $ipActivationStrategy->isHandlerActivated($record);
        // Will return FALSE, Handler will not be triggered
        $this->assertFalse($result);


        $ips = [
            '193.2.33.1/8',
            '193.168.33.20'
        ];
        $ipActivationStrategy = new IpActivationStrategy($requestStack,$ips,100,null);
        $result = $ipActivationStrategy->isHandlerActivated($record);
        $this->assertFalse($result);


        $ips = [
            '190.2.33.1/8',
            '193.168.33.20'
        ];
        $ipActivationStrategy = new IpActivationStrategy($requestStack,$ips,100,null);
        $result = $ipActivationStrategy->isHandlerActivated($record);
        $this->assertTrue($result);

    }

    public function testIpFilterWithRangesAnd404s()
    {
        $requestStack = $this->createMock(RequestStack::class);
        $request = $this->createMock(Request::class);
        $request->method('getClientIp')->willReturn('193.168.10.1');
        $request->method('getPathInfo')->willReturn('/blog');
        $requestStack->method('getMasterRequest')->willReturn($request);

        $urls = [
            '^/blog',
            '^/wp',
        ];

        $notFound = new NotFoundActivationStrategy($requestStack, $urls, 100);

        $ips = [
            '193.168.33.100',
            '193.168.33.101',
            '193.168.33.100',
            '193.168.33.102'
        ];

        $ipActivationStrategy = new IpActivationStrategy($requestStack,$ips,100,$notFound);
        $record = $this->buildRecord();
        $result = $ipActivationStrategy->isHandlerActivated($record);
        $this->assertFalse($result);
    }

    public function testIpFilterNoMatchingAndPathInfoNotMatching()
    {
        $requestStack = $this->createMock(RequestStack::class);
        $request = $this->createMock(Request::class);
        $request->method('getClientIp')->willReturn('193.168.10.1');
        $request->method('getPathInfo')->willReturn('/blog');
        $requestStack->method('getMasterRequest')->willReturn($request);

        $urls = [
            '^/blog-test',
            '^/wp',
        ];

        $notFound = new NotFoundActivationStrategy($requestStack, $urls, 100);

        $ips = [
            '193.168.33.100',
            '193.168.33.101',
            '193.168.33.100',
            '193.168.33.102'
        ];

        $ipActivationStrategy = new IpActivationStrategy($requestStack,$ips,100,$notFound);
        $record = $this->buildRecord();
        $result = $ipActivationStrategy->isHandlerActivated($record);
        $this->assertTrue($result);
    }

    public function testIpFilterMatchingAndPathInfoNotMatching()
    {
        $requestStack = $this->createMock(RequestStack::class);
        $request = $this->createMock(Request::class);
        $request->method('getClientIp')->willReturn('193.168.10.1');
        $request->method('getPathInfo')->willReturn('/blog');
        $requestStack->method('getMasterRequest')->willReturn($request);

        $urls = [
            '^/blog-test',
            '^/wp',
        ];

        $notFound = new NotFoundActivationStrategy($requestStack, $urls, 100);

        $ips = [
            '193.168.33.100/16',
            '193.168.33.101',
            '193.168.33.100',
            '193.168.33.102'
        ];

        $ipActivationStrategy = new IpActivationStrategy($requestStack,$ips,100,$notFound);
        $record = $this->buildRecord();
        $result = $ipActivationStrategy->isHandlerActivated($record);
        $this->assertFalse($result);
    }

    /**
     * @return array
     */
    protected function buildRecord(): array
    {
        $record = [];
        $record['message'] = "Uncaught PHP Exception Symfony\Component\HttpKernel\Exception\NotFoundHttpException: ";
        $record['context']['exception'] = new NotFoundHttpException();
        $record['level'] = 400;
        $record['level_name'] = "ERROR";
        $record['channel'] = "request";
        $record['datetime'] = new \DateTime();
        $record['extra'] = [];
        return $record;
    }
}